import React from 'react'
import {Link} from "react-router-dom"

const CardOptions = function (props) {
    return (
        <div className="col-md-3 cursorPointer cardOptions mt-2">
            <Link to={props.link}>
            <div className="card">
                <div className="card-body text-center">
                    <i className={"fas fa-3x fa-"+props.icon}></i>
                    <br />
                    {props.title}
                </div>
            </div>
            </Link>
        </div>
    )
}

export default CardOptions;