import React, { Component } from 'react'
import {SliderService} from "./../../services"

export default class ItemsSlider extends Component {
    constructor(props){
        super(props);
        this.sliderservice = new SliderService()
        this.state = {
            description : ''
        }
    }
    componentDidMount = () =>{
        var description = this.props.description;
        var longitud = description.length;
        if (longitud > 72) {
            description = description.substr(0,73)+'...'
        }
        this.setState({
            description:description
        })
    }
    render() {
        return (
            <div className="col-md-4 mt-3">
                <div className="card">
                    <img src={this.props.image} className="card-img-top" alt="..." />
                    <div className="card-body">
                        <h5 className="card-title">{this.props.title}</h5>
                        <h6 className="card-subtitle mb-2 text-muted">{this.props.subtitle}</h6>
                        <p className="card-text">{this.state.description}</p>
                        <button onClick={() => this.props.actionEliminar(this.props.id)} className="card-link btn btn-sm btn-danger"><i className="fa fa-trash"></i> Eliminar</button>
                        <a href="#" className="card-link btn btn-sm btn-primary"><i className="fa fa-pen"></i> Actualizar</a>
                    </div>
                </div>
            </div>
        )
    }
}
