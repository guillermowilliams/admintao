import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Switch} from "react-router-dom";

import ScrollToTop from "./../components/other/ScrollToTop"

import Ingreso from "./../views/Ingreso"
import Dashboard from "./../views/Dashboard"
import Slider from "./../views/Slider"
import Especialidad from "./../views/Especialidad"
import Blog from "./../views/Blog"
import Miscelena from "./../views/Miscelena"
import Registro from "./../views/Registro"
import Login from "./../views/Login"

export default class MyRouter extends Component {
  render() {
    return (
      <div className="MyWebApp">
        <Router>
            <ScrollToTop>
            <div className="main">
                <Switch>
                    <Route path="/" exact component={Ingreso} />
                    <Route path="/login123" exact component={Login} />
                    <Route path="/dashboard" exact component={Dashboard} />
                    <Route path="/slider" exact component={Slider} />
                    <Route path="/especialidad" exact component={Especialidad} />
                    <Route path="/blog" exact component={Blog} />
                    <Route path="/miscelena" exact component={Miscelena} />
                    <Route path="/registro/:option" exact component={Registro} />
                </Switch>
            </div>
            </ScrollToTop>
        </Router>
      </div>
    )
  }
}
