import React, { Component } from 'react';
import $ from "jquery"
import Alert from 'react-s-alert';
import IMGLogo from "./../assets/images/logo/logo2018.png"

export default class componentName extends Component {
    constructor(props){
        super(props);
        this.state = {
            hora:'',
            minutes:'',
            seconds:'',
            apostrofe:'',
            dia:'',
            mes:'',
            anio:'',
            textDia: ''
        }
    }
    componentWillMount = () =>{
        window.document.title = 'LC - Asistencia de personal'
        var thes = this;
        setInterval(function(){
            thes.fnMostrarHora();
        },1000)
        this.fnMostrarFecha();
    }
    fnMostrarHora = () =>{
        var marcacion = '';
        var Hora = '';
        var Minutos = '';
        var Segundos = '';
        var dn = '';
        marcacion = new Date()
        Hora = marcacion.getHours()
        Minutos = marcacion.getMinutes()
        Segundos = marcacion.getSeconds()
        /*variable para el apóstrofe de am o pm*/
        dn = "a.m"
        if (Hora > 12) {
        dn = "p.m"
        Hora = Hora - 12
        }
        if (Hora == 0)
        Hora = 12
        /* Si la Hora, los Minutos o los Segundos son Menores o igual a 9, le añadimos un 0 */
        if (Hora <= 9) Hora = "0" + Hora
        if (Minutos <= 9) Minutos = "0" + Minutos
        if (Segundos <= 9) Segundos = "0" + Segundos

        this.setState({
            hora:Hora,
            minutes:Minutos,
            seconds:Segundos,
            apostrofe:dn,
        })
    }
    fnMostrarFecha = () =>{
        var Dia = new Array("Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo");
        var Mes = new Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", 
        "Octubre", "Noviembre", "Diciembre");
        var Hoy = new Date();
        var Anio = Hoy.getFullYear();
        var Fecha = Dia[Hoy.getDay()] + ", " + Hoy.getDate() + " de " + Mes[Hoy.getMonth()] + " de " + Anio + ". ";
        this.setState({
            textDia:Fecha,
            dia:Hoy.getDate(),
            mes:Hoy.getMonth(),
            anio:Anio
        })
    }
    fnMarcacion = () => {
        $('.btn-register')
        .removeClass('btn-primary')
        .addClass('btn-success')
        .attr('disabled','true')
        .html('<i class="fas fa-spinner fa-spin"></i> &nbsp; Cargando...')
        setTimeout(function(){

            Alert.success('<i class="fa fa-user"></i> Guillermo Juica Ruidias<br>06/05/2019 11:50:36 p.m', {
                position: 'top-left',
                html:true,
                effect: 'scale',
                onShow: function () {
                    console.log('aye!')
                },
                beep: true,
                timeout: 'none',
                offset: 100
            });

            $('.btn-register')
            .removeClass('btn-success')
            .addClass('btn-primary')
            .removeAttr('disabled')
            .html('<i class="fa fa-check"></i> &nbsp; Marcar')
        },3000)
    }
    render() {
        return (
            <div className="section-ingreso">
                <Alert stack={true} timeout={3000} />
                <div className="content-form-ingreso">
                    <div className="content-logo" style={{backgroundImage:`url(${IMGLogo})`}}></div>
                    <div className="content-fecha">
                        {this.state.textDia}
                    </div>
                    <div className="content-hora">
                        {this.state.hora+':'+this.state.minutes+':'+this.state.seconds+' '+this.state.apostrofe}
                    </div>
                    <div className="content-formulario mt-4">
                        <div className="form-group">
                            <input className="form-control" placeholder="Ingrese código."/>
                        </div>
                        <div className="form-group mt-4">
                            <button className={"btn btn-primary btn-rounded btn-register"} onClick={() => this.fnMarcacion()}><i className="fa fa-check"></i> &nbsp; Marcar</button>
                        </div>
                    </div>
                </div>
            </div>   
        );
    }
}
