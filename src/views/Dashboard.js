import React, { Component } from 'react'
import WebLayout from "./../layout/WebLayout"
import CardOption from "./../components/Dashboard/CardOptions"

export default class Dashboard extends Component {
    render() {
        return (
            <WebLayout 
                section="dashboard"
                iconPage="tachometer-alt"
                titlePage="Dashboard"
                msgInicial="Bienvenido"
                msgSecundario="Guillermo Juica Ruidias."
                url=""
            >
                <div className="row mt-3">
                    <div className="col-md-12">
                        <div className="alert alert-warning alert-dismissible fade show" role="alert">
                            <strong>Seleccione una opción!</strong> Las siguientes opciones presentan 1 modulo para su respectivo mantenimiento.
                            <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                    <CardOption title="Slider principal" icon="images" link="/slider"/>
                    <CardOption title="Especialidad" icon="hard-hat" link="/especialidad"/>
                    <CardOption title="Blog" icon="bullhorn" link="/blog"/>
                    <CardOption title="Miscelena" icon="headset" link="/miscelena"/>
                    <CardOption title="Administración de usuarios" icon="users" link="/usuarios"/>
                    <CardOption title="Cuenta" icon="cog" link="/configuracion"/>
                </div>
            </WebLayout>
        )
    }
}
