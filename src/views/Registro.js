import React, { Component } from 'react'
import WebLayout from "./../layout/WebLayout"

export default class Registro extends Component {
    render() {
        return (
            <WebLayout
                section="registro"
                iconPage="images"
                titlePage="Registro"
                msgInicial="&nbsp;"
                msgSecundario="&nbsp;"
                url=""
            >
                
            </WebLayout>
        )
    }
}
