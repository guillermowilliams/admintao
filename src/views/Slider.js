import React, { Component } from 'react'
import {Link} from "react-router-dom"
import Masonry from 'react-masonry-component';
import WebLayout from "./../layout/WebLayout"
import ItemSlider from "./../components/Slider/ItemsSlider"
import Swal from 'sweetalert2'
import {SliderService} from "./../services"

export default class Slider extends Component {
    constructor(props){
        super(props);
        this.sliderservice = new SliderService()
        this.state = {
            slider: []
        }
    }
    componentDidMount = () =>{
        this.fnGetData();
    }
    fnGetData = () =>{
        this.sliderservice.retrieve().then(({data})=>{
            this.setState({slider: data})
        }).catch(({response})=>{
            console.error(response)
        })
    }
    fnEliminar = (id) =>{
        Swal.fire({
            title: 'Eliminar item',
            text: '¿Estas seguro que deseas eliminar este item?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Si, eliminar!',
            cancelButtonText: 'No, cancelar'
        }).then((result) => {
            if (result.value) {
                this.sliderservice.delete(id).then(({data})=>{
                    if (data.eliminado == 'eliminado') {
                        Swal.fire(
                            'Eliminado!',
                            'El item ah sido eliminado.',
                            'success'
                        )
                        this.fnGetData();
                    }
                }).catch(({response})=>{
                    console.log(response)
                })
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                
            }
        })
    }
    render() {
        return (
            <WebLayout 
                section="slider"
                iconPage="images"
                titlePage="Slider"
                msgInicial="&nbsp;"
                msgSecundario="&nbsp;"
                url=""
            >
                <div className="row mt-3">
                    <div className="col-md-12">
                        <Link to={"/registro/slider"} className="btn btn-primary float-right"><i className="fas fa-plus"></i>&nbsp;Agregar nuevo Slider</Link>
                    </div>
                </div>
            
                <Masonry>
                    {
                        this.state.slider.map((fill,index) =>
                            <ItemSlider 
                                key={index}
                                image={"http://sente.jjuica.com/"+fill.img_slider} 
                                title={fill.title_slider}
                                subtitle={fill.subtitle_slider}
                                description={fill.desc_slider}
                                id={fill.id_slider}
                                actionEliminar={this.fnEliminar}
                            />
                        )
                    }
                </Masonry>

            </WebLayout>
        )
    }
}
