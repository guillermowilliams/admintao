import React, { Component } from 'react'

export default class Login extends Component {
    render() {
        return (
            <div className="section-login">
                <div className="position-form">
                    <div className="card">
                        <div className="card-body">
                            <div className="content-form">
                                <div className="input-group mb-3 input-group-alternative">
                                    <div className="input-group-prepend">
                                        <span className="input-group-text" id="basic-addon1"><i className="fa fa-user"></i></span>
                                    </div>
                                    <input type="text" className="form-control" placeholder="Usuario" aria-label="Username" aria-describedby="basic-addon1" />
                                </div>
                                <div className="input-group mb-3 input-group-alternative">
                                    <div className="input-group-prepend">
                                        <span className="input-group-text" id="basic-addon1"><i className="fa fa-key"></i></span>
                                    </div>
                                    <input type="password" className="form-control" placeholder="Contraseña" aria-label="password" aria-describedby="basic-addon1" />
                                </div>
                                <div className="form-group">
                                    <div className="custom-control custom-control-alternative custom-checkbox">
                                        <input className="custom-control-input" id=" customCheckLogin" type="checkbox" />
                                        <label className="custom-control-label" for=" customCheckLogin">
                                            <span className="text-muted">&nbsp;Recordar usuario</span>
                                        </label>
                                    </div>
                                </div>
                                <div className="form-group text-center">
                                    <button type="button" className="btn btn-primary my-4 btn-rounded">Ingresar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
