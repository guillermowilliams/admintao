import React from 'react'

const Footer = function () {
    return (
        <div className="myFooter mt-3 mb-3">
            <div className="container text-center">
                &copy; <a href="mailto:guillermojuica3@gmail.com">Guillermo Juica Ruidias </a> - <a href="https://api.whatsapp.com/send?phone=51946591615" target="_blank">946 591 615</a>
            </div>
        </div>
    )
}

export default Footer;