import React, { Component } from 'react'
import Header from "./Header"
import Footer from "./Footer"

export default class WebLayout extends Component {
    componentWillMount = () =>{
        document.title = 'Tao - '+this.props.titlePage
    }
    render() {
        return (
            <div>
            <Header />
            <div className={"section-"+this.props.section}>
                <div className="container">
                    <div className="row">
                        <div className="col-md-12 mt-4">
                            <h5><i className={"fas fa-"+this.props.iconPage}></i> {this.props.titlePage}</h5>
                            <div className="card mt-4">
                                <div className="card-body">
                                    <h5>{this.props.msgInicial} </h5>
                                    {this.props.msgSecundario}
                                    <div className="float-right" style={{marginTop:'-3%'}}>
                                        <a href={"https://jjuica.com"+this.props.url} target="_blank" className="btn btn-primary">
                                            <i className="fas fa-globe"></i> 
                                            &nbsp;Visitar sitio
                                        </a>
                                    </div>
                                    <hr />

                                    {this.props.children}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
            </div>
        )
    }
}
