import React, { Component } from 'react'
import {NavLink} from "react-router-dom"

export default class Menu extends Component {
  render() {
    return (
        <ul className="navbar-nav mr-auto">
            <li className="nav-item">
                <NavLink className="nav-link" to={"/"} exact activeClassName="active">
                    Dashboard
                </NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link" to={"/slider"} activeClassName="active">
                    Slider principal
                </NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link" to={"/especialidad"} activeClassName="active">
                    Especialidad
                </NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link" to={"/blog"} activeClassName="active">
                    Blog
                </NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link" to={"/miscelena"} activeClassName="active">
                    Miscelena
                </NavLink>
            </li>
        </ul>
    )
  }
}
