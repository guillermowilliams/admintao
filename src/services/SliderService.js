import axios from "axios";
import {API_HOST} from "./../config/config"

export class SliderService {
    constructor() {}
    retrieve() {
        return axios.get(`${API_HOST}`,{
            params: {
                page: 'MySlider',
                action: 'MySlider',
                method: 'get_slider'
            }
        }).then(response => response);
    }
    delete(id) {
        return axios.delete(`${API_HOST}`,{
            params: {
                page: 'MySlider',
                action: 'MySlider',
                method: 'crud_delete_slider',
                p_id: id
            }
        });
    }
    /*create(payload, config) {
        return axios.post(IMAGES_ENDPOINT, payload, config);
    }
    update(id, payload) {
        return axios.put(`${IMAGES_ENDPOINT}${id}`, payload);
    }
    delete(id) {
        return axios.delete(`${IMAGES_ENDPOINT}${id}`);
    }*/
}